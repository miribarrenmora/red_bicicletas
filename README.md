# Bienvenido a Express
# Descripción del proyecto

Se trabaja del lado servidor, en el backend, desarrollando el soporte que toda aplicación necesita para lidiar con la persistencia de la información, 
el setup de un servidor web, la creación de una API REST, autenticación y autorización, y la integración de librerías de terceros. 
Utiliza Express para el servidor web, y una base de datos NoSQL orientada a documentos: MongoDB.
ODM con Mongoose y las típicas tareas CRUD sobre Mongo.

# Instalación
Ingresa a la terminal:

npm install

Se instalarán todas las dependencias necesarias para el proyecto.